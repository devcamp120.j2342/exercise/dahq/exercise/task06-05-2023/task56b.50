package com.devcamp.customeraccountapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainAccount {
    @CrossOrigin
    @GetMapping("/accounts")
    public ArrayList<Account> accountsList() {
        Customer customer1 = new Customer(1, "Nguyen Van A", 10);
        Customer customer2 = new Customer(2, "Nguyen Van B", 20);
        Customer customer3 = new Customer(3, "Nguyen VAn C", 30);
        System.out.println(customer1.toString());
        System.out.println(customer2.toString());
        System.out.println(customer3.toString());

        Account account1 = new Account(1, 1000, customer1);
        Account account2 = new Account(2, 1700, customer2);
        Account account3 = new Account(3, 6700, customer3);
        System.out.println(account1.toString());
        System.out.println(account2.toString());
        System.out.println(account3.toString());
        account2 = account2.deposit(500);
        account1 = account1.deposit(500);
        System.out.println(account2.toString());
        ArrayList<Account> accounts = new ArrayList<>();
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);

        return accounts;

    }

}
