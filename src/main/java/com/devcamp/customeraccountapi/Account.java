package com.devcamp.customeraccountapi;

public class Account {
    private int id;
    private double balance = 0;
    private Customer customer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Account(int id, double balance, Customer customer) {
        this.id = id;
        this.balance = balance;
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "Account [id=" + id + ", balance=" + balance + "$" + ", customer=" + customer + "]";
    }

    public Account deposit(double amount) {
        Account account = new Account(id, balance, customer);
        account.balance = account.balance + amount;
        return account;

    }

    public Account withdraw(double amount) {
        Account account = new Account(id, balance, customer);
        if (amount <= account.balance) {

            account.balance = account.balance - amount;

        } else {
            System.out.println("amount excceded to balance");
        }
        return account;

    }

}
